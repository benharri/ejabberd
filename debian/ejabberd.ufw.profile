[ejabberd]
title=XMPP server (client-to-server)
description=robust, scalable and extensible realtime platform (XMPP server + MQTT broker + SIP service)
ports=5222/tcp

[ejabberd SSL]
title=XMPP server (client-to-server-ssl)
description=robust, scalable and extensible realtime platform (XMPP server + MQTT broker + SIP service)
ports=5223/tcp

[ejabberd S2S]
title=XMPP server (server-to-server)
description=robust, scalable and extensible realtime platform (XMPP server + MQTT broker + SIP service)
ports=5269/tcp
