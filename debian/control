Source: ejabberd
Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
Uploaders: Konstantin Khomoutov <flatworm@users.sourceforge.net>,
           Philipp Huebner <debalance@debian.org>,
           Rhonda D'Vine <rhonda@debian.org>
Section: net
Priority: optional
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-apparmor,
               libsqlite3-dev,
               po-debconf,
               rebar,
               erlang-asn1,
               erlang-base,
               erlang-base64url (>= 1.0.1),
               erlang-crypto,
               erlang-dev,
               erlang-eunit,
               erlang-goldrush (>= 0.1.9),
               erlang-idna (>= 6.0.0),
               erlang-inets,
               erlang-jiffy (>= 1.1.1),
               erlang-jose (>= 1.11.1),
               erlang-lager (>= 3.9.1),
               erlang-luerl,
               erlang-mnesia,
               erlang-p1-acme (>= 1.0.19),
               erlang-p1-cache-tab (>= 1.0.30),
               erlang-p1-eimp (>= 1.0.22),
               erlang-p1-mqtree (>= 1.0.15),
               erlang-p1-mysql (>= 1.0.19),
               erlang-p1-oauth2 (>= 0.6.11),
               erlang-p1-pkix (>= 1.0.9),
               erlang-p1-pam (>= 1.0.12),
               erlang-p1-pgsql (>= 1.1.18),
               erlang-p1-sip (>= 1.0.47),
               erlang-p1-sqlite3 (>= 1.1.13),
               erlang-p1-stringprep (>= 1.0.28),
               erlang-p1-stun (>= 1.2.2),
               erlang-p1-tls (>= 1.1.15),
               erlang-p1-utils (>= 1.0.25),
               erlang-p1-xml (>= 1.1.49),
               erlang-p1-xmpp (>= 1.5.8),
               erlang-p1-yaml (>= 1.0.33),
               erlang-p1-yconf (>= 1.0.13),
               erlang-p1-zlib (>= 1.0.12),
               erlang-parsetools,
               erlang-redis-client (>= 1.2.0),
               erlang-ssl,
               erlang-unicode-util-compat (>= 0.3.1),
               erlang-xmerl
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/ejabberd-packaging-team/ejabberd
Vcs-Git: https://salsa.debian.org/ejabberd-packaging-team/ejabberd.git
Homepage: https://www.ejabberd.im

Package: ejabberd
Architecture: any
Multi-Arch: allowed
Depends: adduser,
         ucf,
         ${misc:Depends},
         ${shlibs:Depends},
         lsb-base,
         erlang-base | ${erlang-abi:Depends},
         ${erlang:Depends},
         erlang-base64url (>= 1.0.1),
         erlang-goldrush (>= 0.1.9),
         erlang-idna (>= 6.0.0),
         erlang-jiffy (>= 1.1.1),
         erlang-jose (>= 1.11.1),
         erlang-lager (>= 3.9.1),
         erlang-os-mon,
         erlang-p1-acme (>= 1.0.19),
         erlang-p1-cache-tab (>= 1.0.30),
         erlang-p1-eimp (>= 1.0.22),
         erlang-p1-mqtree (>= 1.0.15),
         erlang-p1-pkix (>= 1.0.9),
         erlang-p1-stringprep (>= 1.0.28),
         erlang-p1-stun (>= 1.2.2),
         erlang-p1-tls (>= 1.1.15),
         erlang-p1-utils (>= 1.0.25),
         erlang-p1-xml (>= 1.1.49),
         erlang-p1-xmpp (>= 1.5.8),
         erlang-p1-yaml (>= 1.0.33),
         erlang-p1-yconf (>= 1.0.13),
         erlang-p1-zlib (>= 1.0.12),
         erlang-unicode-util-compat (>= 0.3.1),
         erlang-xmerl
Suggests: apparmor,
          apparmor-utils,
          libunix-syslog-perl,
          imagemagick,
          yamllint,
          ejabberd-contrib,
          erlang-luerl,
          erlang-p1-oauth2 (>= 0.6.11),
          erlang-p1-mysql (>= 1.0.19),
          erlang-p1-pam (>= 1.0.12),
          erlang-p1-pgsql (>= 1.1.18),
          erlang-p1-sip (>= 1.0.47),
          erlang-p1-sqlite3 (>= 1.1.13),
          erlang-redis-client (>= 1.2.0)
Pre-Depends: ${misc:Pre-Depends}, openssl
Provides: mqtt-broker,
          stun-server,
          turn-server,
          xmpp-server
Description: extensible realtime platform (XMPP server + MQTT broker + SIP service)
 ejabberd is a Jabber/XMPP + MQTT + SIP server written in Erlang, featuring:
  * distributed operation with load-balancing across a cluster;
  * fault-tolerant database replication and storage on multiple nodes,
    allowing nodes to be added or replaced "on the fly";
  * virtual hosting (several virtual domains can be served using a single
    ejabberd instance);
  * XMPP compliance;
  * MQTT 5 compliance;
  * SIP service;
  * web-based administration;
  * SSL/TLS support;
  * conferencing via Multi-User Chat;
  * Jabber Users Directory, based on users' vCards;
  * service discovery;
  * shared roster.
