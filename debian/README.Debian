Table of contents
=================

1 Running ejabberd
1.1 Getting started
1.2 Using privileged TCP ports
1.2 Custom options for Erlang emulator
1.3 Backups
1.4 Using IPv6
1.5 Using captcha
2 Hostname issues
3 PAM authentication
4 Using DBMS for data storage
5 External modules
6 Upgrading from earlier versions


1 Running ejabberd
==================


1.1 Getting started
-------------------

For a quick start, simply execute `dpkg-reconfigure ejabberd` and fill out the
prompts according to your wishes.


1.2 Using privileged TCP ports
------------------------------

The ejabberd daemon is owned by user ejabberd,
so it cannot bind to privileged ports (below 1024). The error message in
/var/log/ejabberd/ejabberd.log looks similar to the following:

E(<0.242.0>:ejabberd_listener:80):
                 Failed to open socket for {443,
                                            ejabberd_c2s,
                                            [{access,c2s},
                                             {max_stanza_size,
                                                 65536},
                                             starttls,
                                             {certfile,
                                                 "/etc/ejabberd/ejabberd.pem"},
                                             {shaper,
                                                 c2s_shaper}]}: eacces

If you want ejabberd to listen for example port 443 you could redirect it to
port 5222 (the default XMPP port) by a firewall rule. For example:

iptables -t nat -A PREROUTING -p tcp --dport 443 -j REDIRECT --to-port 5222


1.3 Custom options for Erlang emulator
--------------------------------------

You can add extra options to the erl command line like

  -pa /usr/local/lib/ejabberd/ebin

by adding them to ERL_OPTIONS in /etc/default/ejabberd directly or through
executing `dpkg-reconfigure ejabberd`.


1.4 Backups
-----------

The package always makes a backup of the ejabberd database if it's running
when it is upgraded or removed. The backup files are stored in the
/var/backups/ejabberd-<timestamp>.* directory.

To make a backup yourself, either run
 /var/lib/dpkg/info/ejabberd.prerm upgrade
or something like
 mkdir /var/backups/ejabberd-backup-manual
 chown ejabberd:ejabberd /var/backups/ejabberd-backup-manual
 ejabberdctl backup \
   /var/backups/ejabberd-backup-manual/ejabberd-database-"$(date +%Y-%m-%dT%H:%M:%S)"


1.5 Using IPv6
--------------

Since ejabberd 14.07-2, IPv6 is enabled by default, as it is for the Debian kernel.
The IPv6 listeners also handle IPv4, so all is well.

If you want to go back to IPv4 only, replace
 'ip: "::"' with 'ip: "0.0.0.0"'
for all listeners in ejabberd.yml.


1.6 Using captchas
------------------

The captcha generation script uses the "convert" program from the "imagemagick"
package. As it depends on a lot of packages related to graphics and
X Window System, it is listed as suggested. Therefore, if you intend to
enable captcha support for your multi-user conferences be sure to install
imagemagick besides enabling captcha in the ejabberd configuration file.
Also the "graphicsmagick-imagemagick-compat" package can be installed
instead of "imagemagick" -- it provides the "convert" program compatible
with that of imagemagick.


2 Hostname issues
=================

Since ejabberd 17.08-1, the default ERLANG_NODE name is ejabberd@localhost,
which makes it easy to relocate ejabberd to a different host.

In earlier versions the Mnesia database used by ejabberd was tightly bound to
the hostname because the ERLANG_NODE name was ejabberd@<hostname>.
Here, special care is needed if for some reason the hostname changes.

Further information can be found in /usr/share/doc/ejabberd/NEWS.Debian.gz.


3 PAM authentication
====================

ejabberd uses the "epam" binary from the erlang-p1-pam package as a helper
program to perform PAM authentication.
Depending on the PAM plugins used, the process accessing these plugins
might have to be run with elevated privileges. The official ejabberd
Installation and Operation Guide simplemindedly suggests to make epam
setuid root which would make it work for any possible case.
Debian's erlang-p1-pam package, on the other hand, ships the epam program
with the setuid and setgid bits cleared, and here is why:
* Setuid and setgid programs present a serious potential security
  risk and are a target for exploitation attempts (especially
  programs which are made setuid root).
* Different PAM plugins have different requirements for the privileges
  of the user working with them, for instance, to use the shadow passwords
  database for authentication, it is sufficient for the accessing process
  to be in the group "shadow"; to use LDAP backend, no elevation of
  privileges is needed at all.
Taking into account the fact that ejabberd uses internal authentication
by default (using its own private database), the decision of what
special access rights to set for the epam program is left to the
system administrator.

To maintain non-standard access rights for the epam binary, it is
advised to use dpkg-statoverride, as suggested by the Debian policy
(section 10.9 "Permissions and owners") -- ejabberd package configuration
scripts honor this setting so it survives upgrades.

As an example, to make epam setgid shadow, you can use:

  dpkg-statoverride --add root shadow 02755 /usr/lib/erlang/p1_pam/bin/epam

and to make epam setuid root you can do:

  dpkg-statoverride --add root ejabberd 04754 /usr/lib/erlang/p1_pam/bin/epam

(Note that we set o-x there so that only users in the ejabberd group
are able to elevate their privileges by executing this program.)

When ejabberd is started via systemd, you will have to edit the
ejabberd.service file and set the following:

 [Service]
 PrivateDevices=false
 NoNewPrivileges=false

Otherwise it will be impossible for the epam helper program to gain the
configured setuid and/or setgid privileges.

With systemd >= 218 you can simply create an override with
`systemctl edit ejabberd.service` and put
 [Service]
 PrivateDevices=
 PrivateDevices=false
 NoNewPrivileges=
 NoNewPrivileges=false
in there.

With systemd < 218 the changes must be made manually.
In order to not loose the changes upon upgrades,
remove the symlink
 /etc/systemd/system/multi-user.target.wants/ejabberd.service
and replace it with a copy of
 /lib/systemd/system/ejabberd.service
before making the changes.
Remember to do a `systemctl daemon-reload` afterwards.


4 Using DBMS for data storage
=============================

The ejabberd Installation and Operation Guide mentions that ejabberd
is able to use various DBMS engines to manage most of ejabberd's data,
such as user accounts, rosters, offline messages, pubsub storage etc.

Starting with ejabberd 14.07-1, all officially supported DBMS engines
are supported through the Debian package, as ejabberd is now compiled
and installed with support for ODBC, MySQL and PostgreSQL.
You have to install the according erlang-* package listed in the
"Suggests" section of ejabberd though.

Note that it appears that currently the only way to use Microsoft SQL
Server as a database backend is via ODBC (and the FreeTDS ODBC driver).


5 External modules
==================

Several 3rd-party modules from https://github.com/processone/ejabberd-contrib
are available through the ejabberd-contrib package or individually as
ejabberd-mod-<module name>.
Not all modules from the github repository are currently packaged,
if you need more, please contact the package maintainer.

If you want to develop your own module, the ejabberd-contrib repository is
the best place to start. Please see the Readme.md or scroll downwards on
https://github.com/processone/ejabberd-contrib

All header files are available there as well as through the debian packages.

To install a custom module, copy the resulting beam file(s) to a directory
in which the Erlang emulator running ejabberd looks for compiled
files.
The best way to handle external modules is to maintain a special directory
for them under "/usr/local", e.g. "/usr/local/lib/ejabberd/ebin".
To make Erlang emulator know about this directory, see 1.3.
See the erl(1) manual page for more info on the "-pa" Erlang emnulator
command line option.


6 Upgrading from earlier versions
=================================

See https://docs.ejabberd.im/admin/upgrade/.


Authors
=======

Torsten Werner <twerner@debian.org>
Sergei Golovan <sgolovan@debian.org>
Konstantin Khomoutov <flatworm@users.sourceforge.net>
Philipp Huebner <debalance@debian.org>

Sat, 15 Jan 2022 12:10:28 +0100
